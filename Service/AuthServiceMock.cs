using MostrArti.Interfaces;

namespace MostrArti.Service
{
    public class AuthServiceMock : IAuthService
    {
        public bool authorized(string username, string password)
        {
            return true;
        }

        public void refreshToken(string username, string refreshToken)
        {
            //Refresh token
        }
    }
}