﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MostrArti.Interfaces
{
    interface IAuthService
    {
        bool authorized(string username, string password);
        void refreshToken(string username, string refreshToken);

    }
}
