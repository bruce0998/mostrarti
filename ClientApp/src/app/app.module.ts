import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { JwtModule } from "@auth0/angular-jwt";
import {
  MatDialogModule,
  MAT_DIALOG_DEFAULT_OPTIONS,
} from "@angular/material/dialog";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatButtonModule } from "@angular/material/button";

import { AppComponent } from "./app.component";
import { NavMenuComponent } from "./nav-menu/nav-menu.component";
import { HomeComponent } from "./pages/home/home.component";
import { MenuComponent } from "./components/menu/menu.component";
import { AuthService } from "./services/auth-services";
import { DataService } from "./services/data-services";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AuthGuard } from "./guards/auth-guard.service";
import { DialogConfirmationComponent } from "./components/alert/dialog-confirmation-component";
import { GalleriaComponent } from './pages/galleria/galleria.component';
import { MostraComponent } from './pages/mostra/mostra.component';
import { OperaComponent } from './pages/opera/opera.component';
import { LoginDialogComponent } from "./components/login/login-dialog.component";
import { ProfileComponent } from './pages/profile/profile.component';
import { StoricoComponent } from './pages/storico/storico.component';
import { StudioComponent } from './pages/studio/studio.component';
import { FooterComponent } from './components/footer/footer.component';
import { RegistrazioneComponent } from './components/registrazione/registrazione.component';
import { AddMostraDialogComponent } from './components/add-mostra-dialog/add-mostra-dialog.component';
import { CaricaOperaDialogComponent } from './components/carica-opera-dialog/carica-opera-dialog.component';


export function tokenGetter() {
  return localStorage.getItem("jwt");
}

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    MenuComponent,
    DialogConfirmationComponent,
    GalleriaComponent,
    MostraComponent,
    OperaComponent,
    LoginDialogComponent,
    ProfileComponent,
    StoricoComponent,
    StudioComponent,
    FooterComponent,
    RegistrazioneComponent,
    AddMostraDialogComponent,
    CaricaOperaDialogComponent
  ],
  entryComponents: [DialogConfirmationComponent, LoginDialogComponent, RegistrazioneComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: "ng-cli-universal" }),
    HttpClientModule,
    FormsModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    RouterModule.forRoot([
      {
        path: "",
        component: GalleriaComponent,
        pathMatch: "full",
      },
      { path: "menu", component: MenuComponent },
      { path: "mostra", component: MostraComponent },
      { path: "opera", component: OperaComponent },
      { path: "profilo", component: ProfileComponent },
      { path: "storico", component: StoricoComponent },
      { path: "studio", component: StudioComponent }
    ]),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: [
          "localhost:5000"
        ],
        blacklistedRoutes: [],
      },
    }),

    MatDialogModule,
    FormsModule,
    BrowserAnimationsModule,
  ],
  exports: [RouterModule],
  providers: [AuthService, DataService, AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
