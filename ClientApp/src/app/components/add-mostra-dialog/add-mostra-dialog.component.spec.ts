import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMostraDialogComponent } from './add-mostra-dialog.component';

describe('AddMostraDialogComponent', () => {
  let component: AddMostraDialogComponent;
  let fixture: ComponentFixture<AddMostraDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddMostraDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMostraDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
