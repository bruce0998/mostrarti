import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DataService } from "src/app/services/data-services";

export interface AddMostraData {}

@Component({
  selector: "app-add-mostra-dialog",
  templateUrl: "./add-mostra-dialog.component.html",
  styleUrls: ["./add-mostra-dialog.component.css"],
})
export class AddMostraDialogComponent implements OnInit {
  constructor(
    public ds: DataService,
    public dialogRef: MatDialogRef<AddMostraDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AddMostraData
  ) {}

  ngOnInit(): void {}

  onOkClick(): void {
    this.dialogRef.close(true);
  }

  onCancelClick(): void {
    this.dialogRef.close(false);
  }
}
