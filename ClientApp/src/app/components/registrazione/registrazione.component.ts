import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DataService } from "src/app/services/data-services";
import { LoginData } from "../login/login-dialog.component";

@Component({
  selector: "app-registrazione",
  templateUrl: "./registrazione.component.html",
  styleUrls: ["./registrazione.component.css"],
})
export class RegistrazioneComponent implements OnInit {
  constructor(
    public ds: DataService,
    public dialogRef: MatDialogRef<RegistrazioneComponent>,
    @Inject(MAT_DIALOG_DATA) public data: LoginData
  ) {}

  ngOnInit(): void {}

  onRegClick(): void {
    this.ds.logged = true;
    this.dialogRef.close(true);
  }

  onCancelClick(): void {
    this.dialogRef.close(false);
  }
}
