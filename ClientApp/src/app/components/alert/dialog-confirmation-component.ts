import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

export interface DialogConfirmationData {
    title: string;
    question: string;
    validation: boolean;
    notValidateMessage: string;
    leftButton: string;
    rightButton: string;
    singleButton: boolean;
  }
  @Component({
    selector: "app-dialog-confirmation-component",
    templateUrl: "dialog-confirmation-component.html",
    styleUrls: ["./dialog-confirmation-component.css"],
  })
  export class DialogConfirmationComponent {
    constructor(
      public dialogRef: MatDialogRef<DialogConfirmationComponent>,
      @Inject(MAT_DIALOG_DATA) public data: DialogConfirmationData
    ) {}
  
    onNoClick(): void {
      this.dialogRef.close(false);
    }
  
    onOkClick(): void {
      this.dialogRef.close(true);
    }
  }
  