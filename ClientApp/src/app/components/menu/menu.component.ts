import { Component } from "@angular/core";
import { DataService } from "../../services/data-services";
import { AuthService } from "../../services/auth-services";
import { DialogConfirmationComponent } from "../alert/dialog-confirmation-component";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: "menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"],
})
export class MenuComponent {
  constructor(public auth: AuthService, private dialog: MatDialog) {}

  logout() {
    const dialogRef = this.dialog.open(DialogConfirmationComponent, {
      width: "350px",
      data: {
        title: "Esci",
        question: "Sicuro di voler uscire?",
        validation: true,
        leftButton: "Annulla",
        rightButton: "Conferma",
        singleButton: false,
      },
    });

    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.auth.logOut();
      }
    });
  }
}
