import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DataService } from "src/app/services/data-services";

export interface CaricaOperaData {}

@Component({
  selector: "app-carica-opera-dialog",
  templateUrl: "./carica-opera-dialog.component.html",
  styleUrls: ["./carica-opera-dialog.component.css"],
})
export class CaricaOperaDialogComponent implements OnInit {
  constructor(
    public ds: DataService,
    public dialogRef: MatDialogRef<CaricaOperaDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CaricaOperaData
  ) {}

  ngOnInit(): void {}

  onOkClick(): void {
    this.dialogRef.close(true);
  }

  onCancelClick(): void {
    this.dialogRef.close(false);
  }
}
