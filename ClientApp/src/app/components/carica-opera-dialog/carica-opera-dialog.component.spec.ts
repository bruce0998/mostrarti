import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaricaOperaDialogComponent } from './carica-opera-dialog.component';

describe('CaricaOperaDialogComponent', () => {
  let component: CaricaOperaDialogComponent;
  let fixture: ComponentFixture<CaricaOperaDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaricaOperaDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaricaOperaDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
