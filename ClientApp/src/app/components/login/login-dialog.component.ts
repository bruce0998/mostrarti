import { Component, Inject, OnInit } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material/dialog";
import { DataService } from "src/app/services/data-services";
import { RegistrazioneComponent } from "../registrazione/registrazione.component";

export interface LoginData {}

@Component({
  selector: "app-login-dialog",
  templateUrl: "./login-dialog.component.html",
  styleUrls: ["./login-dialog.component.css"],
})
export class LoginDialogComponent implements OnInit {
  constructor(
    public ds: DataService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<LoginDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: LoginData
  ) {}

  ngOnInit(): void {}

  registrati() {
    this.dialogRef.close(false);
    const dialogRegRef = this.dialog.open(RegistrazioneComponent, {
      width: "650px",
      data: {},
    });

    dialogRegRef.afterClosed().subscribe((res) => {
      if (res) {
      }
      console.log(res);
    });
  }

  onLoginClick(): void {
    this.ds.logged = true;
    this.dialogRef.close(true);
  }

  onCancelClick(): void {
    this.dialogRef.close(false);
  }
}
