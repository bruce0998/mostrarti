import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { CaricaOperaDialogComponent } from "src/app/components/carica-opera-dialog/carica-opera-dialog.component";
import { DataService } from "src/app/services/data-services";

@Component({
  selector: "app-studio",
  templateUrl: "./studio.component.html",
  styleUrls: ["./studio.component.css"],
})
export class StudioComponent implements OnInit {
  constructor(public ds: DataService, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.ds.home = false;
  }

  carica() {
    const dialogRef = this.dialog.open(CaricaOperaDialogComponent, {
      width: "850px",
      data: {},
      maxHeight: "90vh",
    });

    dialogRef.afterClosed().subscribe((res) => {
      console.log(res);
    });
  }
}
