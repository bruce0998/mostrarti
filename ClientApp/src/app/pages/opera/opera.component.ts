import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data-services';

@Component({
  selector: 'app-opera',
  templateUrl: './opera.component.html',
  styleUrls: ['./opera.component.css']
})
export class OperaComponent implements OnInit {

  constructor(public ds: DataService) { }

  ngOnInit(): void {
    this.ds.home = false;
  }

}
