import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth-services';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  isLoaded: boolean = false;

  constructor(private router: Router, public auth: AuthService) { }

  login(form: NgForm) {
    this.auth.login(form.value);
    this.router.navigateByUrl("/");
  }

  appear(){
    console.log("Logo loaded");
    this.isLoaded = true;
  }
}
