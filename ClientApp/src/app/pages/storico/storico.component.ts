import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { AddMostraDialogComponent } from "src/app/components/add-mostra-dialog/add-mostra-dialog.component";
import { DataService } from "src/app/services/data-services";

@Component({
  selector: "app-storico",
  templateUrl: "./storico.component.html",
  styleUrls: ["./storico.component.css"],
})
export class StoricoComponent implements OnInit {
  constructor(public ds: DataService, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.ds.home = false;
  }

  allestisci() {
    const dialogRef = this.dialog.open(AddMostraDialogComponent, {
      width: "850px",
      data: {},
      maxHeight: '90vh' 
    });

    dialogRef.afterClosed().subscribe((res) => {
      console.log(res);
    })
  }
}
