import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { DataService } from "src/app/services/data-services";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent {
  constructor(
    private router: Router,
    public ds: DataService,
  ) {}
}
