import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data-services';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(public ds: DataService) { }

  ngOnInit(): void {
    this.ds.home = false;
  }


}
