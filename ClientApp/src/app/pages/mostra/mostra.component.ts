import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data-services';

@Component({
  selector: 'app-mostra',
  templateUrl: './mostra.component.html',
  styleUrls: ['./mostra.component.css']
})
export class MostraComponent implements OnInit {

  constructor(public ds: DataService) { }

  ngOnInit(): void {
    this.ds.home = false;
  }

}
