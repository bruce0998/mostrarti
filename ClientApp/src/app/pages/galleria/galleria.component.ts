import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data-services';

@Component({
  selector: 'app-galleria',
  templateUrl: './galleria.component.html',
  styleUrls: ['./galleria.component.css']
})
export class GalleriaComponent implements OnInit {

  constructor(public ds: DataService) { }

  ngOnInit(): void {
    this.ds.home = true;
  }

  scrollLeft(el: Element) {
    el.scrollLeft += 300;
  }

  scrollRight(el: Element) {
    el.scrollLeft -= 300;
  }

}
