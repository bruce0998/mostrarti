"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TipoCassa = exports.SantiniTipo = exports.StatoCivile = exports.TipoSepoltura = exports.TipoCofano = exports.TipoDocumento = exports.Parentela = exports.PreventivoDefunto = exports.PraticaCliente = exports.Documento = exports.DocData = exports.ItemReplace = void 0;
var ItemReplace = /** @class */ (function () {
    function ItemReplace() {
    }
    return ItemReplace;
}());
exports.ItemReplace = ItemReplace;
var DocData = /** @class */ (function () {
    function DocData() {
    }
    return DocData;
}());
exports.DocData = DocData;
var Documento = /** @class */ (function () {
    function Documento() {
    }
    return Documento;
}());
exports.Documento = Documento;
var PraticaCliente = /** @class */ (function () {
    function PraticaCliente() {
    }
    PraticaCliente.prototype.isValid = function () {
        if (this.nome &&
            this.cognome &&
            this.via &&
            this.cap &&
            this.citta &&
            this.telefono &&
            this.codicefiscale &&
            this.email &&
            this.ddt &&
            this.fattura &&
            this.numerodocumento &&
            this.tipoDocumento &&
            this.parentela != undefined) {
            return true;
        }
        return false;
    };
    return PraticaCliente;
}());
exports.PraticaCliente = PraticaCliente;
var PreventivoDefunto = /** @class */ (function () {
    function PreventivoDefunto() {
    }
    PreventivoDefunto.prototype.isValid = function () {
        if (this.nome &&
            this.cognome &&
            this.natoIl &&
            this.natoCitta &&
            this.natoProvincia &&
            this.statoCivile &&
            this.professione &&
            this.cittaResidenza &&
            this.viaResidenza &&
            this.dataOraMorte &&
            this.mortoDove &&
            this.cameraMortuaria &&
            this.dataOraChiusuraCassa &&
            this.benedizione &&
            this.sMessaOre &&
            this.parrocchia &&
            this.sepoltura &&
            ((this.tipoSepoltura === TipoSepoltura.Cremazione &&
                this.cremazioneDove &&
                this.dispersioneDove &&
                this.affidoDove &&
                this.tumulazioneCeneriDove &&
                this.ritiroCeneriQuando &&
                this.consegnaCeneriQuando &&
                this.consegnaCeneriDove) ||
                (this.tipoSepoltura === TipoSepoltura.Tumulazione &&
                    this.tumulazioneDove) ||
                (this.tipoSepoltura === TipoSepoltura.Inumazione &&
                    this.inumazioneDove)) &&
            this.cassa &&
            this.cassa &&
            this.zinco &&
            this.valvola &&
            this.materasso &&
            this.barriera &&
            this.maniglie &&
            this.croce &&
            this.imbottitura &&
            this.velo &&
            this.cartellino &&
            this.vestiti &&
            this.cassaPreparataDa &&
            this.tipoCofano &&
            this.autoFunebre &&
            this.trasportoCM &&
            this.carroFiori &&
            this.bigliettiRingraziamenti &&
            this.bigliettiRingraziamentiNumero &&
            this.santiniTpo &&
            this.santiniNumero &&
            this.fraseNumero &&
            this.fraseTesto &&
            this.manifesti &&
            this.necrologie &&
            this.cassettaOfferte &&
            this.libroFirme &&
            this.fiori &&
            this.constatazioneFeretro &&
            this.vestizioneSalma &&
            this.allestimentoCameraArdente &&
            this.tasse &&
            this.varie1 &&
            this.varie2 &&
            this.varie3 &&
            this.valoriBollati &&
            this.disbrigoPratiche &&
            this.provvisorio &&
            this.altro) {
            return true;
        }
        return false;
    };
    return PreventivoDefunto;
}());
exports.PreventivoDefunto = PreventivoDefunto;
var Parentela;
(function (Parentela) {
    Parentela[Parentela["Genitore"] = 0] = "Genitore";
    Parentela[Parentela["Figlio"] = 1] = "Figlio";
    Parentela[Parentela["FratelloSorella"] = 2] = "FratelloSorella";
    Parentela[Parentela["Coniuge"] = 3] = "Coniuge";
    Parentela[Parentela["Nonno"] = 4] = "Nonno";
    Parentela[Parentela["Nipote"] = 5] = "Nipote";
    Parentela[Parentela["Suocero"] = 6] = "Suocero";
    Parentela[Parentela["Cognato"] = 7] = "Cognato";
    Parentela[Parentela["Zio"] = 8] = "Zio";
    Parentela[Parentela["Cugino"] = 9] = "Cugino";
    Parentela[Parentela["Nessuno"] = 10] = "Nessuno";
})(Parentela = exports.Parentela || (exports.Parentela = {}));
var TipoDocumento;
(function (TipoDocumento) {
    TipoDocumento[TipoDocumento["CartaIdentita"] = 0] = "CartaIdentita";
    TipoDocumento[TipoDocumento["Passaporto"] = 1] = "Passaporto";
    TipoDocumento[TipoDocumento["Patente"] = 2] = "Patente";
})(TipoDocumento = exports.TipoDocumento || (exports.TipoDocumento = {}));
var TipoCofano;
(function (TipoCofano) {
    TipoCofano[TipoCofano["Corto"] = 0] = "Corto";
    TipoCofano[TipoCofano["Lungo"] = 1] = "Lungo";
})(TipoCofano = exports.TipoCofano || (exports.TipoCofano = {}));
var TipoSepoltura;
(function (TipoSepoltura) {
    TipoSepoltura[TipoSepoltura["Tumulazione"] = 0] = "Tumulazione";
    TipoSepoltura[TipoSepoltura["Inumazione"] = 1] = "Inumazione";
    TipoSepoltura[TipoSepoltura["Cremazione"] = 2] = "Cremazione";
})(TipoSepoltura = exports.TipoSepoltura || (exports.TipoSepoltura = {}));
var StatoCivile;
(function (StatoCivile) {
    StatoCivile[StatoCivile["CelibeNubile"] = 0] = "CelibeNubile";
    StatoCivile[StatoCivile["Coniugato"] = 1] = "Coniugato";
    StatoCivile[StatoCivile["Vedovo"] = 2] = "Vedovo";
    StatoCivile[StatoCivile["Divorziato"] = 3] = "Divorziato";
    StatoCivile[StatoCivile["NonClassificabile"] = 4] = "NonClassificabile";
    StatoCivile[StatoCivile["UnitoCivilmente"] = 5] = "UnitoCivilmente";
    StatoCivile[StatoCivile["Libero"] = 6] = "Libero";
})(StatoCivile = exports.StatoCivile || (exports.StatoCivile = {}));
var SantiniTipo;
(function (SantiniTipo) {
    SantiniTipo[SantiniTipo["tipo1"] = 0] = "tipo1";
    SantiniTipo[SantiniTipo["tipo2"] = 1] = "tipo2";
})(SantiniTipo = exports.SantiniTipo || (exports.SantiniTipo = {}));
var TipoCassa;
(function (TipoCassa) {
    TipoCassa[TipoCassa["tipo1"] = 0] = "tipo1";
    TipoCassa[TipoCassa["tipo2"] = 1] = "tipo2";
})(TipoCassa = exports.TipoCassa || (exports.TipoCassa = {}));
//# sourceMappingURL=model.js.map