"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = exports.tokenGetter = void 0;
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/common/http");
var router_1 = require("@angular/router");
var angular_jwt_1 = require("@auth0/angular-jwt");
var dialog_1 = require("@angular/material/dialog");
var progress_bar_1 = require("@angular/material/progress-bar");
var progress_spinner_1 = require("@angular/material/progress-spinner");
var button_1 = require("@angular/material/button");
var app_component_1 = require("./app.component");
var nav_menu_component_1 = require("./nav-menu/nav-menu.component");
var home_component_1 = require("./pages/home/home.component");
var users_component_1 = require("./users/users.component");
var login_component_1 = require("./pages/login/login.component");
var menu_component_1 = require("./components/menu/menu.component");
var auth_services_1 = require("./services/auth-services");
var data_services_1 = require("./services/data-services");
var animations_1 = require("@angular/platform-browser/animations");
var auth_guard_service_1 = require("./guards/auth-guard.service");
var dialog_confirmation_component_1 = require("./components/alert/dialog-confirmation-component");
function tokenGetter() {
    return localStorage.getItem("jwt");
}
exports.tokenGetter = tokenGetter;
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                nav_menu_component_1.NavMenuComponent,
                home_component_1.HomeComponent,
                users_component_1.UsersComponent,
                login_component_1.LoginComponent,
                menu_component_1.MenuComponent,
                dialog_confirmation_component_1.DialogConfirmationComponent,
            ],
            entryComponents: [dialog_confirmation_component_1.DialogConfirmationComponent],
            imports: [
                platform_browser_1.BrowserModule.withServerTransition({ appId: "ng-cli-universal" }),
                http_1.HttpClientModule,
                forms_1.FormsModule,
                progress_bar_1.MatProgressBarModule,
                progress_spinner_1.MatProgressSpinnerModule,
                button_1.MatButtonModule,
                router_1.RouterModule.forRoot([
                    {
                        path: "",
                        component: home_component_1.HomeComponent,
                        pathMatch: "full",
                    },
                    { path: "users", component: users_component_1.UsersComponent },
                    { path: "login", component: login_component_1.LoginComponent },
                    { path: "menu", component: menu_component_1.MenuComponent },
                ]),
                angular_jwt_1.JwtModule.forRoot({
                    config: {
                        tokenGetter: tokenGetter,
                        whitelistedDomains: [
                            "localhost:5000"
                        ],
                        blacklistedRoutes: [],
                    },
                }),
                dialog_1.MatDialogModule,
                forms_1.FormsModule,
                animations_1.BrowserAnimationsModule,
            ],
            exports: [router_1.RouterModule],
            providers: [auth_services_1.AuthService, data_services_1.DataService, auth_guard_service_1.AuthGuard],
            bootstrap: [app_component_1.AppComponent],
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map