import { Component } from '@angular/core';
import { AuthService } from './services/auth-services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'app';
  
  constructor(private auth: AuthService) { }
  isAuthenticated() {
    return this.auth.isAuthenticated();
  }
}
