"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataService = void 0;
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var rx = require("rxjs");
var moment = require("moment");
var DataService = /** @class */ (function () {
    function DataService(auth, http) {
        var _this = this;
        this.auth = auth;
        this.http = http;
        this.loadingValues$ = new rxjs_1.BehaviorSubject(new Set());
        this.loadingValues = new Set();
        this.lastLoading = 0;
        this.practices$ = new rxjs_1.BehaviorSubject(null);
        this.fileSystem$ = new rxjs_1.BehaviorSubject(null);
        this.baseUrl = window.location.protocol + "//" + window.location.host + "/";
        this.querying = false;
        var self = this;
        this.dataUpdater = new rxjs_1.BehaviorSubject(0);
        this.configUpdater = new rxjs_1.BehaviorSubject(0);
        this.loadingObs = this.loadingValues$.pipe(operators_1.map(function (v) {
            return v.size !== 0;
        }));
        this.doUpdateDataFunction = rx.pipe(operators_1.delay(1000), operators_1.finalize(function () {
            _this.dataUpdater.next(moment.now());
        }));
        this.doUpdateConfigFunction = rx.pipe(operators_1.delay(1000), operators_1.finalize(function () {
            _this.configUpdater.next(moment.now());
        }));
        this.loadingObs.pipe(
        // Se arriva un not loading, aspetto 1 secondo prima di notificarlo
        operators_1.debounce(function (val) { return !val ? rx.timer(1000) : rx.timer(0); })).subscribe(function (l) {
            self.querying = l;
        });
        //this.auth.getToken()
        //  .pipe(switchMap(_ => this.getPractices())).subscribe(pracs => {
        //    this.practices$.next(pracs);
        //  });
        this.auth.getToken()
            .pipe(operators_1.switchMap(function (_) { return _this.getFileSystem(); })).subscribe(function (fs) {
            var items = fs.map(function (mod) { return ({ documento: mod, checked: false }); });
            _this.fileSystem$.next(items);
        });
    }
    Object.defineProperty(DataService.prototype, "practices", {
        get: function () { return this.practices$.value; },
        set: function (v) { this.practices$.next(v); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataService.prototype, "fileSystem", {
        get: function () { return this.fileSystem$.value; },
        set: function (v) { this.fileSystem$.next(v); },
        enumerable: false,
        configurable: true
    });
    DataService.prototype.peekLoading = function () {
        var ll = this.lastLoading++;
        this.loadingValues.add(ll);
        this.loadingValues$.next(this.loadingValues);
        return ll;
    };
    DataService.prototype.endLoading = function (ll) {
        this.loadingValues.delete(ll);
        this.loadingValues$.next(this.loadingValues);
    };
    DataService.prototype.getPractices = function () {
        var _this = this;
        var loadingToken = this.peekLoading();
        return this.http.get(this.baseUrl + 'api/practice/', {
            headers: new http_1.HttpHeaders({
                "Content-Type": "application/json"
            })
        }).pipe(operators_1.finalize(function () { return _this.endLoading(loadingToken); }));
    };
    ;
    DataService.prototype.getFileSystem = function () {
        var _this = this;
        var loadingToken = this.peekLoading();
        return this.http.get(this.baseUrl + 'api/document/filesystem', {
            headers: new http_1.HttpHeaders({
                "Content-Type": "application/json"
            })
        }).pipe(operators_1.finalize(function () { return _this.endLoading(loadingToken); }));
    };
    DataService.prototype.createPractice = function (praticaCliente, preventivoDefunto) {
        var _this = this;
        var loadingToken = this.peekLoading();
        var data = {
            nomePC: praticaCliente.nome ? praticaCliente.nome : '',
            cognomePC: praticaCliente.cognome ? praticaCliente.cognome : '',
            via: praticaCliente.via ? praticaCliente.via : '',
            cap: praticaCliente.cap ? praticaCliente.cap : '',
            citta: praticaCliente.citta ? praticaCliente.citta : '',
            telefono: praticaCliente.telefono ? praticaCliente.telefono : '',
            codicefiscale: praticaCliente.codicefiscale ? praticaCliente.codicefiscale : '',
            email: praticaCliente.email ? praticaCliente.email : '',
            ddt: praticaCliente.ddt ? praticaCliente.ddt : '',
            fattura: praticaCliente.fattura ? praticaCliente.fattura : '',
            numerodocumento: praticaCliente.numerodocumento ? praticaCliente.numerodocumento : '',
            tipoDocumento: praticaCliente.tipoDocumento ? praticaCliente.tipoDocumento.valueOf : 0,
            parentela: praticaCliente.parentela ? praticaCliente.parentela.valueOf : 0,
            nomePD: preventivoDefunto.nome ? preventivoDefunto.nome : '',
            cognomePD: preventivoDefunto.cognome ? preventivoDefunto.cognome : '',
            natoIl: preventivoDefunto.natoIl ? preventivoDefunto.natoIl : '',
            natoCitta: preventivoDefunto.natoCitta ? preventivoDefunto.natoCitta : '',
            natoProvincia: preventivoDefunto.natoProvincia ? preventivoDefunto.natoProvincia : '',
            statoCivile: preventivoDefunto.statoCivile ? preventivoDefunto.statoCivile.valueOf : 0,
            professione: preventivoDefunto.professione ? preventivoDefunto.professione : '',
            cittaResidenza: preventivoDefunto.cittaResidenza ? preventivoDefunto.cittaResidenza : '',
            viaResidenza: preventivoDefunto.viaResidenza ? preventivoDefunto.viaResidenza : '',
            dataOraMorte: preventivoDefunto.dataOraMorte ? preventivoDefunto.dataOraMorte : '',
            mortoDove: preventivoDefunto.mortoDove ? preventivoDefunto.mortoDove : '',
            cameraMortuaria: preventivoDefunto.cameraMortuaria ? preventivoDefunto.cameraMortuaria : '',
            dataOraChiusuraCassa: preventivoDefunto.dataOraChiusuraCassa ? preventivoDefunto.dataOraChiusuraCassa : '',
            benedizione: preventivoDefunto.benedizione ? preventivoDefunto.benedizione : '',
            sMessaOre: preventivoDefunto.sMessaOre ? preventivoDefunto.sMessaOre : '',
            parrocchia: preventivoDefunto.parrocchia ? preventivoDefunto.parrocchia : '',
            sepoltura: preventivoDefunto.sepoltura ? preventivoDefunto.sepoltura : '',
            tipoSepoltura: preventivoDefunto.tipoSepoltura ? preventivoDefunto.tipoSepoltura.valueOf : 0,
            cremazioneDove: preventivoDefunto.cremazioneDove ? preventivoDefunto.cremazioneDove : '',
            dispersioneDove: preventivoDefunto.dispersioneDove ? preventivoDefunto.dispersioneDove : '',
            affidoDove: preventivoDefunto.affidoDove ? preventivoDefunto.affidoDove : '',
            tumulazioneCeneriDove: preventivoDefunto.tumulazioneCeneriDove ? preventivoDefunto.tumulazioneCeneriDove : '',
            ritiroCeneriQuando: preventivoDefunto.ritiroCeneriQuando ? preventivoDefunto.ritiroCeneriQuando : '',
            consegnaCeneriQuando: preventivoDefunto.consegnaCeneriQuando ? preventivoDefunto.consegnaCeneriQuando : '',
            consegnaCeneriDove: preventivoDefunto.consegnaCeneriDove ? preventivoDefunto.consegnaCeneriDove : '',
            tumulazioneDove: preventivoDefunto.tumulazioneCeneriDove ? preventivoDefunto.tumulazioneCeneriDove : '',
            inumazioneDove: preventivoDefunto.inumazioneDove ? preventivoDefunto.inumazioneDove : '',
            cassa: preventivoDefunto.cassa ? preventivoDefunto.cassa.valueOf : 0,
            zinco: preventivoDefunto.zinco ? preventivoDefunto.zinco : false,
            valvola: preventivoDefunto.valvola ? preventivoDefunto.valvola : false,
            materasso: preventivoDefunto.materasso ? preventivoDefunto.materasso : false,
            barriera: preventivoDefunto.barriera ? preventivoDefunto.barriera : false,
            maniglie: preventivoDefunto.maniglie ? preventivoDefunto.maniglie : '',
            croce: preventivoDefunto.croce ? preventivoDefunto.croce : '',
            imbottitura: preventivoDefunto.imbottitura ? preventivoDefunto.imbottitura : '',
            velo: preventivoDefunto.velo ? preventivoDefunto.velo : '',
            cartellino: preventivoDefunto.cartellino ? preventivoDefunto.cartellino : '',
            vestiti: preventivoDefunto.vestiti ? preventivoDefunto.vestiti : '',
            cassaPreparataDa: preventivoDefunto.cassaPreparataDa ? preventivoDefunto.cassaPreparataDa : '',
            tipoCofano: preventivoDefunto.tipoCofano ? preventivoDefunto.tipoCofano.valueOf : 0,
            autoFunebre: preventivoDefunto.autoFunebre ? preventivoDefunto.autoFunebre : '',
            trasportoCM: preventivoDefunto.trasportoCM ? preventivoDefunto.trasportoCM : '',
            carroFiori: preventivoDefunto.carroFiori ? preventivoDefunto.carroFiori : '',
            bigliettiRingraziamenti: preventivoDefunto.bigliettiRingraziamenti ? preventivoDefunto.bigliettiRingraziamenti : '',
            bigliettiRingraziamentiNumero: preventivoDefunto.bigliettiRingraziamentiNumero ? preventivoDefunto.bigliettiRingraziamentiNumero : 0,
            santiniTpo: preventivoDefunto.santiniTpo ? preventivoDefunto.santiniTpo.valueOf : 0,
            santiniNumero: preventivoDefunto.santiniNumero ? preventivoDefunto.santiniNumero : 0,
            fraseNumero: preventivoDefunto.fraseNumero ? preventivoDefunto.fraseNumero : 0,
            fraseTesto: preventivoDefunto.fraseTesto ? preventivoDefunto.fraseTesto : '',
            manifesti: preventivoDefunto.manifesti ? preventivoDefunto.manifesti : '',
            necrologie: preventivoDefunto.necrologie ? preventivoDefunto.necrologie : '',
            cassettaOfferte: preventivoDefunto.cassettaOfferte ? preventivoDefunto.cassettaOfferte : '',
            libroFirme: preventivoDefunto.libroFirme ? preventivoDefunto.libroFirme : '',
            fiori: preventivoDefunto.fiori ? preventivoDefunto.fiori : '',
            constatazioneFeretro: preventivoDefunto.constatazioneFeretro ? preventivoDefunto.constatazioneFeretro : '',
            vestizioneSalma: preventivoDefunto.vestizioneSalma ? preventivoDefunto.vestizioneSalma : '',
            allestimentoCameraArdente: preventivoDefunto.allestimentoCameraArdente ? preventivoDefunto.allestimentoCameraArdente : '',
            tasse: preventivoDefunto.tasse ? preventivoDefunto.tasse : '',
            varie1: preventivoDefunto.varie1 ? preventivoDefunto.varie1 : '',
            varie2: preventivoDefunto.varie2 ? preventivoDefunto.varie2 : '',
            varie3: preventivoDefunto.varie3 ? preventivoDefunto.varie3 : '',
            valoriBollati: preventivoDefunto.valoriBollati ? preventivoDefunto.valoriBollati : '',
            disbrigoPratiche: preventivoDefunto.disbrigoPratiche ? preventivoDefunto.disbrigoPratiche : '',
            provvisorio: preventivoDefunto.provvisorio ? preventivoDefunto.provvisorio : '',
            altro: preventivoDefunto.altro ? preventivoDefunto.altro : '',
        };
        localStorage.getItem("jwt");
        return this.http.post(this.baseUrl + 'api/practice/add', data, {
            headers: new http_1.HttpHeaders({
                "Content-Type": "application/json"
            })
        }).pipe(this.doUpdateDataFunction, operators_1.finalize(function () { return _this.endLoading(loadingToken); }));
    };
    DataService.prototype.printDocuments = function (documentSelected, id_pratica) {
        var _this = this;
        var loadingToken = this.peekLoading();
        var data = {
            docs: documentSelected,
            id: id_pratica
        };
        localStorage.getItem("jwt");
        return this.http.post(this.baseUrl + 'api/document/save', data, {
            headers: new http_1.HttpHeaders({
                "Content-Type": "application/json"
            })
        }).pipe(this.doUpdateDataFunction, operators_1.finalize(function () { return _this.endLoading(loadingToken); }));
    };
    DataService = __decorate([
        core_1.Injectable()
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;
//# sourceMappingURL=data-services.js.map