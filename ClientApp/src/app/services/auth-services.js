"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
var rxjs_1 = require("rxjs");
//@Injectable()
var AuthService = /** @class */ (function () {
    function AuthService() {
    }
    AuthService.prototype.isAuthenticated = function () {
        return new rxjs_1.Observable(function (subscriber) { subscriber.next(true); });
    };
    return AuthService;
}());
exports.AuthService = AuthService;
//# sourceMappingURL=auth-services.js.map