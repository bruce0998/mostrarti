import { Injectable } from "@angular/core";
import { Observable, BehaviorSubject, MonoTypeOperatorFunction } from "rxjs";
import { finalize, map, debounce, delay } from "rxjs/operators";
import * as rx from "rxjs";
import * as moment from "moment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from "@angular/router";
import { JwtHelperService } from "@auth0/angular-jwt";
import { invalid } from "@angular/compiler/src/render3/view/util";

@Injectable()
export class AuthService {
  token: string;
  apiUrl: string;

  loadingObs: Observable<boolean>;
  dataUpdater: BehaviorSubject<number>;
  configUpdater: BehaviorSubject<number>;
  doUpdateDataFunction: MonoTypeOperatorFunction<any>;
  doUpdateConfigFunction: MonoTypeOperatorFunction<any>;

  baseUrl: string =
    window.location.protocol + "//" + window.location.host + "/";

  private loadingValues$: BehaviorSubject<Set<number>> = new BehaviorSubject(
    new Set()
  );
  private loadingValues: Set<number> = new Set();
  private lastLoading = 0;

  querying = false;
  invalidLogin = false;

  peekLoading() {
    const ll = this.lastLoading++;
    this.loadingValues.add(ll);
    this.loadingValues$.next(this.loadingValues);
    return ll;
  }
  endLoading(ll: number) {
    this.loadingValues.delete(ll);
    this.loadingValues$.next(this.loadingValues);
  }

  constructor(
    private jwtHelper: JwtHelperService,
    private http: HttpClient,
    private router: Router
  ) {
    const self = this;

    this.dataUpdater = new BehaviorSubject(0);
    this.configUpdater = new BehaviorSubject(0);
    this.loadingObs = this.loadingValues$.pipe(
      map((v) => {
        return v.size !== 0;
      })
    );

    this.doUpdateDataFunction = rx.pipe(
      delay(1000),
      finalize(() => {
        this.dataUpdater.next(moment.now());
      })
    );

    this.doUpdateConfigFunction = rx.pipe(
      delay(1000),
      finalize(() => {
        this.configUpdater.next(moment.now());
      })
    );

    this.loadingObs
      .pipe(
        // Se arriva un not loading, aspetto 1 secondo prima di notificarlo
        debounce((val) => (!val ? rx.timer(1000) : rx.timer(0)))
      )
      .subscribe((l) => {
        self.querying = l;
      });
  }

  isAuthenticated(): boolean {
    const token: string = localStorage.getItem("jwt");
    if (
      token &&
      !this.jwtHelper.isTokenExpired(token) &&
      token !== "unauthorized"
    ) {
      return true;
    } else {
      return false;
    }
  }

  public login(credentials) {
    const loadingToken = this.peekLoading();
    const jsonData = JSON.stringify(credentials);
    this.http
      .post(this.baseUrl + "api/auth/login", jsonData, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      })
      .pipe(
        this.doUpdateDataFunction,
        finalize(() => this.endLoading(loadingToken))
      )
      .subscribe(
        (response) => {
          const token = (<any>response).token;
          const refreshToken = (<any>response).refreshToken;
          localStorage.setItem("jwt", token);
          localStorage.setItem("refreshToken", refreshToken);
        },
        (err) => {
          this.invalidLogin = true;
        }
      );
  }

  public logOut() {
    localStorage.removeItem("jwt");
    localStorage.removeItem("refreshToken");
  }

  public getToken(): Observable<string> {
    return rx.of(this.jwtHelper.tokenGetter());
  }
}
