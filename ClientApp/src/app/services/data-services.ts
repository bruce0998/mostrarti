import { Observable, BehaviorSubject, MonoTypeOperatorFunction } from "rxjs";
import { finalize, map, debounce, delay } from "rxjs/operators";
import { AuthService } from "./auth-services";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";

import * as rx from "rxjs";
import * as moment from "moment";

@Injectable()
export class DataService {
  loadingObs: Observable<boolean>;
  dataUpdater: BehaviorSubject<number>;
  configUpdater: BehaviorSubject<number>;
  doUpdateDataFunction: MonoTypeOperatorFunction<any>;
  doUpdateConfigFunction: MonoTypeOperatorFunction<any>;

  private loadingValues$: BehaviorSubject<Set<number>> = new BehaviorSubject(
    new Set()
  );
  private loadingValues: Set<number> = new Set();
  private lastLoading = 0;

  baseUrl: string =
    window.location.protocol + "//" + window.location.host + "/";

  querying = false;
  logged = false;
  home = true;

  get practices() {
    return this.practices$.value;
  }
  set practices(v) {
    this.practices$.next(v);
  }
  practices$: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  toUpdate: boolean = false;

  peekLoading() {
    const ll = this.lastLoading++;
    this.loadingValues.add(ll);
    this.loadingValues$.next(this.loadingValues);
    return ll;
  }

  endLoading(ll: number) {
    this.loadingValues.delete(ll);
    this.loadingValues$.next(this.loadingValues);
  }
  constructor(public auth: AuthService, private http: HttpClient) {
    const self = this;

    this.dataUpdater = new BehaviorSubject(0);
    this.configUpdater = new BehaviorSubject(0);
    this.loadingObs = this.loadingValues$.pipe(
      map((v) => {
        return v.size !== 0;
      })
    );

    this.doUpdateDataFunction = rx.pipe(
      delay(1000),
      finalize(() => {
        this.dataUpdater.next(moment.now());
      })
    );

    this.doUpdateConfigFunction = rx.pipe(
      delay(1000),
      finalize(() => {
        this.configUpdater.next(moment.now());
      })
    );

    this.loadingObs
      .pipe(
        // Se arriva un not loading, aspetto 1 secondo prima di notificarlo
        debounce((val) => (!val ? rx.timer(1000) : rx.timer(0)))
      )
      .subscribe((l) => {
        self.querying = l;
      });

    this.auth.getToken().subscribe((_) => {
      //Controllo se il token esiste ed eventualmente carico i dati
    });
  }

  /*
        ███████╗███████╗███████╗███╗   ███╗██████╗ ██╗     ██████╗ ███████╗████████╗    
        ██╔════╝██╔════╝██╔════╝████╗ ████║██╔══██╗██║    ██╔════╝ ██╔════╝╚══██╔══╝    
        █████╗  ███████╗█████╗  ██╔████╔██║██████╔╝██║    ██║  ███╗█████╗     ██║       
        ██╔══╝  ╚════██║██╔══╝  ██║╚██╔╝██║██╔═══╝ ██║    ██║   ██║██╔══╝     ██║       
        ███████╗███████║███████╗██║ ╚═╝ ██║██║     ██║    ╚██████╔╝███████╗   ██║       
        ╚══════╝╚══════╝╚══════╝╚═╝     ╚═╝╚═╝     ╚═╝     ╚═════╝ ╚══════╝   ╚═╝       
                                                                                        
        ██╗  ██╗████████╗████████╗██████╗     ██████╗ ███████╗███████╗████████╗         
        ██║  ██║╚══██╔══╝╚══██╔══╝██╔══██╗    ██╔══██╗██╔════╝██╔════╝╚══██╔══╝         
        ███████║   ██║      ██║   ██████╔╝    ██████╔╝█████╗  ███████╗   ██║            
        ██╔══██║   ██║      ██║   ██╔═══╝     ██╔══██╗██╔══╝  ╚════██║   ██║            
        ██║  ██║   ██║      ██║   ██║         ██║  ██║███████╗███████║   ██║            
        ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝         ╚═╝  ╚═╝╚══════╝╚══════╝   ╚═╝            
                                                                                        
  */

  // getPracticeFromId(id: number) {
  //   const loadingToken = this.peekLoading();
  //   this.http
  //     .get(this.baseUrl + "api/practice/" + id, {
  //       headers: new HttpHeaders({
  //         "Content-Type": "application/json",
  //       }),
  //     })
  //     .pipe(
  //       this.doUpdateDataFunction,
  //       finalize(() => this.endLoading(loadingToken))
  //     )
  //     .subscribe((prac) => {
  //       console.log("Trovata la pratica con id: " + id);
  //       console.log(prac);
  //       this.praticaCliente = prac.pc as PraticaCliente;
  //       this.getDocumentsData(
  //         prac.pc.pC_Id,
  //         prac.pc.cli_Nome,
  //         prac.pc.cli_Cognome,
  //         new Date(prac.pc.pra_DataCreate).getFullYear().toString()
  //       ).subscribe((docsData) => {
  //         console.log("Documenti trovati sul server...");
  //         console.log(docsData);
  //       });
  //     });
  // }

  // getPractices() {
  //   const loadingToken = this.peekLoading();
  //   this.http
  //     .get(this.baseUrl + "api/practice/", {
  //       headers: new HttpHeaders({
  //         "Content-Type": "application/json",
  //       }),
  //     })
  //     .pipe(
  //       this.doUpdateDataFunction,
  //       finalize(() => this.endLoading(loadingToken))
  //     )
  //     .subscribe((pracs) => {
  //       console.log("Trovate le seguenti pratiche:");
  //       console.log(pracs);
  //       this.practices$.next(pracs);
  //     });
  // }

  // getDocumentsData(
  //   numeroPratica: number,
  //   nomeCliente: string,
  //   cognomeCliente: string,
  //   anno: string
  // ): Observable<any> {
  //   const loadingToken = this.peekLoading();
  //   var params = new HttpParams()
  //     .set("numeroPratica", numeroPratica.toString())
  //     .set("nomeCliente", nomeCliente)
  //     .set("cognomeCliente", cognomeCliente)
  //     .set("anno", anno);
  //   return this.http
  //     .get(this.baseUrl + "api/document/data", {
  //       params: params,
  //       headers: new HttpHeaders({
  //         "Content-Type": "application/json",
  //       }),
  //     })
  //     .pipe(
  //       this.doUpdateDataFunction,
  //       finalize(() => this.endLoading(loadingToken))
  //     );
  // }
}
