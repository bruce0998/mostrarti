import { Component, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { LoginDialogComponent } from "../components/login/login-dialog.component";
import { DataService } from "../services/data-services";

@Component({
  selector: "app-nav-menu",
  templateUrl: "./nav-menu.component.html",
  styleUrls: ["./nav-menu.component.css"],
})
export class NavMenuComponent {
  constructor(private dialog: MatDialog, public ds: DataService) {}

  login() {
    const dialogRef = this.dialog.open(LoginDialogComponent, {
      width: "450px",
      data: {},
    });

    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
      }
      console.log(res);
    });
  }

  openNav() {
    document.getElementById("mySidebar").style.left="calc(100% - 550px)";
  }

  closeNav() {
    document.getElementById("mySidebar").style.left="100%";
  }
}
