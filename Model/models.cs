﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using static System.Text.Json.JsonElement;

/**
███╗   ███╗ ██████╗ ██████╗ ███████╗██╗                            
████╗ ████║██╔═══██╗██╔══██╗██╔════╝██║                            
██╔████╔██║██║   ██║██║  ██║█████╗  ██║                            
██║╚██╔╝██║██║   ██║██║  ██║██╔══╝  ██║                            
██║ ╚═╝ ██║╚██████╔╝██████╔╝███████╗███████╗                       
╚═╝     ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚══════╝                       
                                                                   
   ███╗   ██╗███████╗████████╗     ██████╗ ██████╗ ██████╗ ███████╗
   ████╗  ██║██╔════╝╚══██╔══╝    ██╔════╝██╔═══██╗██╔══██╗██╔════╝
   ██╔██╗ ██║█████╗     ██║       ██║     ██║   ██║██████╔╝█████╗  
   ██║╚██╗██║██╔══╝     ██║       ██║     ██║   ██║██╔══██╗██╔══╝  
██╗██║ ╚████║███████╗   ██║       ╚██████╗╚██████╔╝██║  ██║███████╗
╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝        ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝
                                                                   
*/

namespace MostrArti.Model
{
    public class OperationResult
    {
        public Boolean success;
        public Boolean Success
        {
            get { return success; }
            set { success = value; }
        }

        public OperationResult(Boolean succ)
        {
            success = succ;
        }

        public static OperationResult ok = new OperationResult(true);
        public static OperationResult fail = new OperationResult(false);
    }
/**
 * Il salt è una protezione in più 
 * sulla password, facile facile che potrebbe fare un figurone
 * (Possibile rimuovere in ogni momento)
 */
    public class User
    {
        [Key]
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string RefreshToken { get; set; }
        public DateTime RefreshTokenExpiryTime { get; set; }
    }
    public class TokenApiModel
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }

}
