﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MostrArti.Interfaces;
using MostrArti.Model;
using MostrArti.Service;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MostrArti.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        readonly ITokenService tokenService;
        IAuthService authService;
        
        public AuthController(IConfiguration config, ITokenService tokenService)
        {
            this.tokenService = tokenService ?? throw new ArgumentNullException(nameof(tokenService));
            this.authService = new AuthServiceMock();
        }

        // POST api/<AuthController>
        [HttpPost, Route("login")]
        public IActionResult Login([FromBody] User user)
        {
            if(user == null)
            {
                return BadRequest("Invalid client request");
            }
            bool authorized = authService.authorized(user.UserName, user.Password);
            if (authorized)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimTypes.Role, "Manager")
                };
                var accessToken = tokenService.GenerateAccessToken(claims);
                var refreshToken = tokenService.GenerateRefreshToken();
                authService.refreshToken(user.UserName, refreshToken);
                return Ok(new { Token = accessToken, RefreshToken = refreshToken });
            } else
            {
                return Unauthorized();
            }
        }
    }
}
